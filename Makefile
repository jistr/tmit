.DEFAULT_GOAL := build
SHELL := /bin/bash
ROOT_DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

CLI_DIR := $(ROOT_DIR)/cli
CONTAINER_DIR := $(ROOT_DIR)/container
LIB_DIR := $(ROOT_DIR)/lib


# TOOLBOX

toolbox-build:
	cd toolbox && \
	podman build -t localhost/tmit-toolbox:latest . && \
	podman tag localhost/tmit-toolbox:latest localhost/tmit-toolbox:$$(date "+%Y_%m_%d")

toolbox-clean:
	podman rmi localhost/tmit-toolbox:latest


# BUILD

BUILD_FLAGS ?= --release

build:
	cd $(LIB_DIR) && cargo build $(BUILD_FLAGS) && \
	cd $(CLI_DIR) && cargo build $(BUILD_FLAGS)

build-cli:
	cd $(CLI_DIR) && cargo build $(BUILD_FLAGS)

clean:
	cd $(LIB_DIR) && cargo clean && \
	cd $(CLI_DIR) && cargo clean

deps-check-outdated:
	if ! cargo outdated -h &> /dev/null; then \
		cargo install cargo-outdated; \
	fi; \
	cd $(LIB_DIR) && cargo outdated && \
	cd $(CLI_DIR) && cargo outdated

deps-update:
	cd $(LIB_DIR) && cargo update && \
	cd $(CLI_DIR) && cargo update

doc: FORCE
	cd $(CLI_DIR) && cargo doc


# TEST

test: test-lib-unit test-lib-func test-cli-unit test-cli-func
	@echo "ALL TESTS OK"

test-lib-unit:
	cd $(LIB_DIR) && cargo test --lib

# using --test-threads 1 because many tests are RW on the cloud and could clash
test-lib-func:
	cd $(LIB_DIR) && \
	if [ ! -e "clouds.yaml" ]; then ln -s ../toolbox/vagrant-devstack/env/clouds.yaml; fi && \
	RUST_BACKTRACE=1 RUST_LOG=debug cargo test --test func_test -- --test-threads 1

test-cli-unit:
	cd $(CLI_DIR) && cargo test --bins

# using --test-threads 1 because many tests are RW on the cloud and could clash
test-cli-func:
	cd $(CLI_DIR) && \
	if [ ! -e "clouds.yaml" ]; then ln -s ../toolbox/vagrant-devstack/env/clouds.yaml; fi && \
	./tests/smoke_test.sh && \
	RUST_BACKTRACE=1 RUST_LOG=debug cargo test --test func_test -- --test-threads 1


# CONTAINER

container-build:
	cd $(CONTAINER_DIR) && \
	buildah bud -t localhost/tmit:latest .

container-quay-trigger:
	bash container/quay-trigger.sh

# force re-run of target
FORCE:
