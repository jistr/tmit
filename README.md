DISCONTINUED PoC
================

We decided to build this OpenStack tenant migration tool differently,
please head over to
[os-migrate](https://github.com/os-migrate/os-migrate).

tmit - OpenStack tenant migration tool
======================================

Running
-------

The easiest way to run Tmit is to run a rootless container. Make sure
you have `clouds.yaml` in the local directory and run this snippet:

    test -e clouds.yaml || echo "Create clouds.yaml here."
    mkdir tmitdata
    alias tmit="podman run --rm -v $PWD/clouds.yaml:/clouds.yaml:z -v $PWD/tmitdata:/tmitdata:z quay.io/jistr/tmit"

Now you have `tmit` command aliased to a rootless container execution
with volume mounts set up so that Tmit can access the `clouds.yaml`
file and the `tmitdata` directory.

Let's see some usage info:

    tmit -h

Let's try exporting the networks:

    tmit export \
        --os-cloud devstack \
        --resource-type network \
        --out-file tmitdata/networks.yml

    cat tmitdata/networks.yml

And subnets:

    tmit export \
        --os-cloud devstack \
        --resource-type subnet \
        --out-file tmitdata/subnets.yml

    cat tmitdata/subnets.yml

Here's how the exported content looks like:
[networks.yml](https://gitlab.com/jistr/tmit/blob/master/cli/test_data/import_network/networks.yml),
[subnets.yml](https://gitlab.com/jistr/tmit/blob/master/cli/test_data/import_subnet/subnets.yml).

In case you hit authentication issues, make sure your clouds.yml is
properly formed, e.g. see
[omission of `project_domain_name`](https://github.com/dtantsur/rust-osauth/issues/9).

Contributing
------------

### End-to-end tests with vagrantized OpenStack

To run end-to-end tests locally, set up a Vagrant box with all-in-one
OpenStack (Devstack) deployment. If you have Vagrant installed on your
machine, `cd toolbox/vagrant-devstack`. If you don't have Vagrant
(e.g. in Fedora Silverblue), you can run a containerized one in a
shell via `./toolbox/vagrant-shell`. In a Vagrant-enabled shell, run:

    # cd toolbox/vagrant-devstack

    # Prepare env and start the devstack VM.
    # (Read the script to see what it does.)
    ./vagrant-up

    # Once the machine is up and configured, you may wish to snapshot
    # it so that you can revert to a clean state later:
    ./vagrant-snapshot-create

    # With snapshot you can stop/start or damage the vagrant VM and
    # revert back to initial state:
    # vagrant halt
    # ./vagrant-up
    # ./vagrant-snapshot-revert

    # If something goes wrong, you can ssh into the VM via:
    # vagrant ssh

Note: when running vagrant from the container, shutting off the
container will power off the VM too.

After a successful `vagrant up`, the `toolbox/vagrant-devstack/env`
directory will contain ssh config and clouds.yaml file which you can
use to talk to the VM and the OpenStack instance even from outside the
Vagrant container.
