use clap::{App, Arg, SubCommand, ArgMatches};
use osauth::Session;
use serde_yaml;
use std::path::Path;
use std::fs::File;

use tmit::ResourceFile;
use tmit::network::{fetch_networks, fetch_subnets};
use tmit::session::create_session;
use crate::{Error, Result};

pub fn create_subcommand<'a, 'b>() -> App<'a, 'b> {
    SubCommand::with_name("export")
        .about("Export resources owned by a tenant")
        .arg(Arg::with_name("os_cloud")
             .long("os-cloud")
             .help("Cloud auth from clouds.yml")
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("resource_type")
             .long("resource-type")
             .help("Type of resource to export")
             .possible_values(&["network", "subnet"])
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("out_file")
             .long("out-file")
             .help("Output resource file")
             .takes_value(true)
             .required(true))
}

pub fn run(_top_matches: &ArgMatches, sub_matches: &ArgMatches) -> Result<()> {
    let os_cloud = sub_matches.value_of("os_cloud")
        .expect("Missing os_cloud parameter value");
    let resource_type = sub_matches.value_of("resource_type")
        .expect("Missing resource_type parameter value");
    let out_file = Path::new(sub_matches.value_of("out_file")
        .expect("Missing out_file parameter value"));
    let session = create_session(&os_cloud)?;

    match resource_type {
        "network" => export_networks(&session, &out_file),
        "subnet" => export_subnets(&session, &out_file),
        _ => Err(Error::input(
            &format!("Unsupported resource type for export: '{}'", &resource_type))),
    }
}

fn export_networks(session: &Session, out_file: &Path) -> Result<()> {
    let networks = fetch_networks(session)?;
    let networks_yml = File::create(&out_file)?;

    let mut res_file = ResourceFile::new_empty();
    res_file.push_networks(networks.into_iter());

    serde_yaml::to_writer(networks_yml, &res_file)?;
    Ok(())
}

fn export_subnets(session: &Session, out_file: &Path) -> Result<()> {
    let subnets = fetch_subnets(session)?;
    let subnets_yml = File::create(&out_file)?;

    let mut res_file = ResourceFile::new_empty();
    res_file.push_subnets(subnets.into_iter());

    serde_yaml::to_writer(subnets_yml, &res_file)?;
    Ok(())
}
