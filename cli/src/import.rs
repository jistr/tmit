use clap::{App, Arg, SubCommand, ArgMatches};
use log::{debug, info, trace};
use osauth::Session;
use serde_yaml;
use std::path::Path;
use std::fs::File;

use tmit::{Resource, ResourceFile};
use tmit::network::{create_network, create_subnet};
use tmit::session::create_session;
use crate::{Error, Result};

const ALL_RESOURCE_TYPES: &'static [&'static str] = &[
    "network",
    "subnet",
];

pub fn create_subcommand<'a, 'b>() -> App<'a, 'b> {
    let res_type_possible_values = [&["all"], ALL_RESOURCE_TYPES].concat();

    SubCommand::with_name("import")
        .about("Import resources as a tenant")
        .arg(Arg::with_name("os_cloud")
             .long("os-cloud")
             .help("Cloud auth from clouds.yml")
             .takes_value(true)
             .required(true))
        .arg(Arg::with_name("resource_type")
             .long("resource-type")
             .help("Limit the imported resources to a particular type.")
             .possible_values(&res_type_possible_values)
             .takes_value(true)
             .default_value("all"))
        .arg(Arg::with_name("in_file")
             .long("in-file")
             .help("Input resource file.")
             .takes_value(true)
             .required(true))
}

pub fn run(_top_matches: &ArgMatches, sub_matches: &ArgMatches) -> Result<()> {
    let os_cloud = sub_matches.value_of("os_cloud")
        .expect("Missing os_cloud parameter value");
    let resource_type = sub_matches.value_of("resource_type")
        .expect("Missing resource_type parameter value");
    let in_file = Path::new(sub_matches.value_of("in_file")
        .expect("Missing in_file parameter value"));
    let session = create_session(&os_cloud)?;
    let resource_type_array = [resource_type];

    let type_filter = match resource_type {
        "all" => ALL_RESOURCE_TYPES,
        _ => &resource_type_array,
    };

    import_resources(&session, &in_file, type_filter)
}

fn import_resources(session: &Session, in_file: &Path, type_filter: &[&str]) -> Result<()> {
    if ! in_file.exists() {
        return Err(Error::input(
            format!("Input file '{}' does not exist.", in_file.display())));
    }

    debug!("Importing resources from '{}' with type filter {:?}", in_file.display(), type_filter);
    let reader = File::open(in_file)?;
    let deserialized: ResourceFile = serde_yaml::from_reader(&reader)?;
    for resource in deserialized.resources {
        trace!("Processing resource {:?}", resource);
        import_resource(session, &resource, type_filter)?;
    }
    Ok(())
}

fn import_resource(session: &Session, resource: &Resource, type_filter: &[&str]) -> Result<()> {
    // TODO: can we DRY the match branches?
    match resource {
        Resource::Network(ref network) => {
            if type_filter.contains(&"network") {
                info!("Importing network '{}'",
                      &network.name.as_ref().map(|s| s.as_str()).unwrap_or(""));
                create_network(session, network)
            } else {
                debug!("Skipping network creation - does not match type filter.");
                Ok(())
            }
        }
        Resource::Subnet(ref subnet) => {
            if type_filter.contains(&"subnet") {
                info!("Importing subnet '{}'",
                      &subnet.name.as_ref().map(|s| s.as_str()).unwrap_or(""));
                create_subnet(session, subnet)
            } else {
                debug!("Skipping subnet creation - does not match type filter.");
                Ok(())
            }
        }
    }.map_err(|e| e.into())
}
