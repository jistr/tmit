mod export;
mod import;
mod result;

use clap::{App, AppSettings, ArgMatches, crate_version};
use env_logger;

pub use crate::result::{Error, ErrorKind, Result};

fn main() {
    init_logging();
    let app = create_app();
    let matches = app.get_matches();
    let result = run(&matches);
    match result {
        Ok(_) => {},
        Err(e) => { eprintln!("Error: {}", e); ::std::process::exit(1); },
    }
}

fn create_app<'a, 'b>() -> App<'a, 'b> {
    App::new("tmit")
        .about("OpenStack tenant migration tool")
        .version(crate_version!())
        .setting(AppSettings::ArgRequiredElseHelp)
        .setting(AppSettings::SubcommandRequired)
        .subcommand(export::create_subcommand())
        .subcommand(import::create_subcommand())
}

fn run(matches: &ArgMatches) -> Result<()> {
    match matches.subcommand() {
        ("export", Some(sub_matches)) => export::run(&matches, &sub_matches),
        ("import", Some(sub_matches)) => import::run(&matches, &sub_matches),
        // Should be unreachable due to AppSettings::SubcommandRequired
        _ => panic!("Missing subcommand"),
    }
}

fn init_logging() {
    env_logger::from_env(
        env_logger::Env::new()
            .filter_or("TMIT_LOG", "info")
            .write_style("TMIT_LOG_STYLE"))
        .init()
}
