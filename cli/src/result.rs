#[derive(Debug, Clone)]
pub enum ErrorKind {
    InputError,
    IoError,
    NotImplemented,
    SerializationError,
    /// Error from Tmit, has further sub-kinds
    TmitError(::tmit::Error),
    __Nonexhaustive,
}

#[derive(Debug, Clone)]
pub struct Error {
    kind: ErrorKind,
    message: String,
}

impl Error {
    pub fn new<S: Into<String>>(kind: ErrorKind, message: S) -> Self {
        Self {
            kind: kind,
            message: message.into(),
        }
    }

    pub fn input<S: Into<String>>(message: S) -> Self {
        Self {
            kind: ErrorKind::InputError,
            message: message.into(),
        }
    }

    pub fn not_implemented<S: Into<String>>(message: S) -> Self {
        Self {
            kind: ErrorKind::NotImplemented,
            message: message.into(),
        }
    }
}

impl ErrorKind {
    pub fn name(&self) -> &'static str {
        match self {
            ErrorKind::TmitError(_) => "TmitError",
            ErrorKind::IoError => "IoError",
            ErrorKind::InputError => "InputError",
            ErrorKind::NotImplemented => "NotImplemented",
            _ => unreachable!(),
        }
    }

    pub fn inner_part_fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        match self {
            ErrorKind::TmitError(inner) => write!(f, " ({})", inner),
            _ => Ok(()),
        }
    }
}

impl ::std::fmt::Display for ErrorKind {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "{}", self.name())?;
        self.inner_part_fmt(f)
    }
}

impl ::std::fmt::Display for Error {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "{}: {}", self.kind.name(), self.message)?;
        self.kind.inner_part_fmt(f)
    }
}

impl ::std::error::Error for Error {
    fn source(&self) -> Option<&(dyn ::std::error::Error + 'static)> {
        match &self.kind {
            ErrorKind::TmitError(ref inner) => Some(inner),
            _ => None,
        }
    }
}

impl From<::serde_yaml::Error> for Error {
    fn from(error: ::serde_yaml::Error) -> Self {
        Self::new(ErrorKind::SerializationError, &format!("{:?}", error))
    }
}

impl From<::std::io::Error> for Error {
    fn from(error: ::std::io::Error) -> Self {
        Self::new(ErrorKind::IoError, &format!("{:?}", error))
    }
}

impl From<::tmit::Error> for Error {
    fn from(error: ::tmit::Error) -> Self {
        Self::new(ErrorKind::TmitError(error), "")
    }
}

pub type Result<T> = ::std::result::Result<T, Error>;
