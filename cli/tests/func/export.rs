use crate::fixture;
use super::util;

#[test]
fn test_export_no_args() {
    let (rc, _stdout, _stderr) = util::cli_run_with_rc(&["export"]);
    assert_eq!(rc, 1, "Expected export with no args to fail");
}

#[test]
fn test_export_network() {
    let (src_session, _dst_session) = util::prep_sessions();
    let file_path = util::make_tmp_dir("export_network").join("networks.yml");
    fixture::clean_network(&[&src_session]);
    fixture::seed_network(&[&src_session]);

    let (stdout, _stderr) = util::cli_run(&[
        "export",
        "--os-cloud", &util::src_cloud_name(),
        "--resource-type", "network",
        "--out-file", &file_path.to_string_lossy(),
    ]);
    assert_eq!(stdout.as_str(), "", "Stdout should be empty");

    let contents = util::read_file(&file_path);
    util::assert_has_substr("tmit_version: ", &contents);
    util::assert_has_line("    name: tmit_net", &contents);
    util::assert_has_line("    mtu: 1350", &contents);
    util::assert_has_line("    port_security_enabled: false", &contents);

    fixture::clean_network(&[&src_session]);
}

#[test]
fn test_export_subnet() {
    let (src_session, _dst_session) = util::prep_sessions();
    let file_path = util::make_tmp_dir("export_subnet").join("subnets.yml");
    fixture::clean_network(&[&src_session]);
    fixture::clean_subnet(&[&src_session]);
    fixture::seed_network(&[&src_session]);
    fixture::seed_subnet(&[&src_session]);

    let (stdout, _stderr) = util::cli_run(&[
        "export",
        "--os-cloud", &util::src_cloud_name(),
        "--resource-type", "subnet",
        "--out-file", &file_path.to_string_lossy(),
    ]);
    assert_eq!(stdout.as_str(), "", "Stdout should be empty");

    let contents = util::read_file(&file_path);
    util::assert_has_substr("tmit_version: ", &contents);
    util::assert_has_line("    name: tmit_subnet", &contents);
    util::assert_has_line("    description: tmit test subnet", &contents);
    util::assert_has_line("    network_name: tmit_net", &contents);
    util::assert_has_line("    cidr: 192.168.5.0/24", &contents);
    util::assert_has_line("    allocation_pools:", &contents);
    util::assert_has_line("      - start: 192.168.5.10", &contents);

    fixture::clean_network(&[&src_session]);
    fixture::clean_subnet(&[&src_session]);
}
