use tmit::network::{self, AllocationPool, Network, Subnet};

use crate::fixture;
use super::util;

#[test]
fn test_import_no_args() {
    let (rc, _stdout, _stderr) = util::cli_run_with_rc(&["import"]);
    assert_eq!(rc, 1, "Expected import with no args to fail");
}

#[test]
fn test_import_not_matching_filter() {
    let (_src_session, dst_session) = util::prep_sessions();
    let file = util::test_data_path("import_network/networks.yml");
    fixture::clean_network(&[&dst_session]);

    let (stdout, _stderr) = util::cli_run(&[
        "import",
        "--os-cloud", &util::dst_cloud_name(),
        "--in-file", &file,
        "--resource-type", "subnet",
    ]);
    assert_eq!(stdout.as_str(), "", "Stdout should be empty");

    let all_nets = network::fetch_networks(&dst_session)
        .expect("Failed to fetch networks from destination cloud");
    let all_tmit_nets: Vec<&Network> = all_nets.iter()
        .filter(|net| net.name.as_ref().expect("Unnamed network found") == "tmit_net")
        .collect();
    assert_eq!(all_tmit_nets.len(), 0,
               "Network tmit_net was imported but it shouldn't have been");

    fixture::clean_network(&[&dst_session]);
}

#[test]
fn test_import_network() {
    let (_src_session, dst_session) = util::prep_sessions();
    let file = util::test_data_path("import_network/networks.yml");
    fixture::clean_network(&[&dst_session]);

    let (stdout, _stderr) = util::cli_run(&[
        "import",
        "--os-cloud", &util::dst_cloud_name(),
        "--in-file", &file,
    ]);
    assert_eq!(stdout.as_str(), "", "Stdout should be empty");

    let all_nets = network::fetch_networks(&dst_session)
        .expect("Failed to fetch networks from destination cloud");
    let all_tmit_nets: Vec<&Network> = all_nets.iter()
        .filter(|net| net.name.as_ref().expect("Unnamed network found") == "tmit_net")
        .collect();
    assert_eq!(all_tmit_nets.len(), 1,
               "Network tmit_net wasn't found in destination cloud exactly once");
    let tmit_net = all_tmit_nets[0];
    assert_eq!(tmit_net.admin_state_up, false);
    assert_eq!(tmit_net.description, Some("tmit test net".into()));
    assert_eq!(tmit_net.external, Some(false));
    assert_eq!(tmit_net.is_default, None);
    assert_eq!(tmit_net.mtu, Some(1350));
    assert_eq!(tmit_net.port_security_enabled, Some(false));

    fixture::clean_network(&[&dst_session]);
}

#[test]
fn test_import_subnet() {
    let (_src_session, dst_session) = util::prep_sessions();
    let file = util::test_data_path("import_subnet/subnets.yml");
    fixture::clean_subnet(&[&dst_session]);
    fixture::clean_network(&[&dst_session]);
    fixture::seed_network(&[&dst_session]);

    let (stdout, _stderr) = util::cli_run(&[
        "import",
        "--os-cloud", &util::dst_cloud_name(),
        "--in-file", &file,
    ]);
    assert_eq!(stdout.as_str(), "", "Stdout should be empty");

    let all_subnets = network::fetch_subnets(&dst_session)
        .expect("Failed to fetch subnets from destination cloud");
    let all_tmit_subnets: Vec<&Subnet> = all_subnets.iter()
        .filter(|subnet| subnet.name.as_ref().expect("Unnamed subnet found") == "tmit_subnet")
        .collect();
    assert_eq!(all_tmit_subnets.len(), 1,
               "Subnet tmit_subnet wasn't found in destination cloud exactly once");
    let tmit_subnet = all_tmit_subnets[0];
    assert_eq!(tmit_subnet.cidr, Some("192.168.5.0/24".parse().unwrap()));
    assert_eq!(tmit_subnet.description, Some("tmit test subnet".into()));
    assert_eq!(tmit_subnet.network_name, Some("tmit_net".into()));
    assert!(tmit_subnet.allocation_pools.contains(
        &AllocationPool {
            start: "192.168.5.10".parse().unwrap(),
            end: "192.168.5.20".parse().unwrap(),
        }));

    fixture::clean_network(&[&dst_session]);
    fixture::clean_subnet(&[&dst_session]);
}
