use env_logger;
use log::debug;
use osauth::{self, Session};
use std::env;
use std::fs::{self, File};
use std::io::Read;
use std::path::{Path, PathBuf};
use std::process::Command;

const CLI_BIN: &'static str = "./target/debug/tmit";
const TEST_DATA_PATH: &'static str = "./test_data";
const TEST_DATA_TMP_PATH: &'static str = "./test_data/tmp";

pub fn cli_run(args: &[&str]) -> (String, String) {
    let (rc, stdout, stderr) = cli_run_with_rc(args);
    assert!(rc == 0,
            "CLI run with args {:?} failed. Stderr: {:?}, Stdout: {:?}", args, stderr, stdout);
    (stdout, stderr)
}

pub fn cli_run_with_rc(args: &[&str]) -> (i32, String, String) {
    let output = Command::new(CLI_BIN)
        .args(args)
        .env("TMIT_LOG", "debug")
        .output()
        .expect("Failed to execute CLI binary");

    // there is no return code when terminated by a signal, default that case to -1
    let rc = output.status.code().unwrap_or(-1);
    let stdout = String::from_utf8(output.stdout).expect("Stdout is not valid UTF-8.");
    let stderr = String::from_utf8(output.stderr).expect("Stderr is not valid UTF-8.");
    debug!("Return code {} from tmit {:?}", rc, args);
    debug!("Stdout: {}", &stdout);
    debug!("Stderr: {}", &stderr);

    (rc, stdout, stderr)
}

pub fn prep_sessions() -> (Session, Session) {
    enable_logging();
    (src_session(), dst_session())
}

pub fn enable_logging() {
    let _ = env_logger::builder().is_test(true).try_init();
}

pub fn src_cloud_name() -> String {
    env::var("TEST_SRC_OS_CLOUD").unwrap_or("devstack".to_string())
}

pub fn dst_cloud_name() -> String {
    env::var("TEST_DST_OS_CLOUD").unwrap_or("devstack-alt".to_string())
}

pub fn src_session() -> Session {
    osauth::from_config(&src_cloud_name())
        .expect("Failed to create test source cloud session.")
}

pub fn dst_session() -> Session {
    osauth::from_config(&dst_cloud_name())
        .expect("Failed to create test destination cloud session.")
}

pub fn test_data_path(sub_path: &str) -> String {
    let dir_path = Path::new(TEST_DATA_PATH).join(sub_path);
    dir_path.to_str().expect("Path is not UTF-8").to_string()
}

pub fn make_tmp_dir(name: &str) -> PathBuf {
    remove_tmp_dir(name);
    let dir_path = Path::new(TEST_DATA_TMP_PATH).join(name);
    fs::create_dir_all(&dir_path).expect("Failed to create test tmp dir");
    dir_path
}

pub fn remove_tmp_dir(name: &str) {
    let dir_path = Path::new(TEST_DATA_TMP_PATH).join(name);
    if dir_path.exists() {
        fs::remove_dir_all(&dir_path).expect("Failed to remove test tmp dir");
    }
}

pub fn read_file(name: &Path) -> String {
    let mut reader = File::open(name)
        .expect(&format!("Failed to open file '{}'", name.display()));
    let mut contents = String::new();
    reader.read_to_string(&mut contents)
        .expect(&format!("Failed to read contents of file '{}'", name.display()));
    contents
}

pub fn assert_has_line(needle: &str, haystack: &str) {
    for line in haystack.lines() {
        if needle == line {
            return;
        }
    }
    panic!("Didn't find line '{}' in string '{}'", needle, haystack);
}

pub fn assert_has_substr(needle: &str, haystack: &str) {
    if haystack.contains(needle) {
        return;
    } else {
        panic!("Didn't find substring '{}' in string '{}'", needle, haystack);
    }
}
