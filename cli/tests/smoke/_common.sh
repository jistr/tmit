if echo "$PWD" | grep -v "/cli$"; then
    echo "Functional tests must be run from the '<tmit repo path>/cli' directory."
    exit 1
fi

CLI_DIR="$PWD"
TMIT="$PWD/target/debug/tmit"

function rebuild_just_once {
    if [ TMIT_CLI_REBUILT != "1" ]; then
        cargo build
        TMIT_CLI_REBUILT=1
    fi
}
