#!/bin/bash

set -euo pipefail

SCRIPT_DIR=$(dirname $(realpath "$0"))
source $SCRIPT_DIR/_common.sh

function test_export_no_args {
    local OUTPUT=$($TMIT export 2>&1 || true)
    grep -q 'required arguments were not provided' <<<"$OUTPUT"
}

function test_export_help {
    local OUTPUT=$($TMIT export -h 2>&1 || true)
    grep -q 'USAGE:' <<<"$OUTPUT"
    grep -q 'OPTIONS:' <<<"$OUTPUT"
    grep -q 'Output resource file' <<<"$OUTPUT"
    grep -q 'Type of resource to export' <<<"$OUTPUT"
}

rebuild_just_once
test_export_no_args
test_export_help
