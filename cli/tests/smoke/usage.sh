#!/bin/bash

set -euo pipefail

SCRIPT_DIR=$(dirname $(realpath "$0"))
source $SCRIPT_DIR/_common.sh

function test_no_args {
    local OUTPUT=$($TMIT 2>&1 || true)
    grep -q 'USAGE:' <<<"$OUTPUT"
    grep -q 'tmit <SUBCOMMAND>' <<<"$OUTPUT"
}

function test_help {
    local OUTPUT=$($TMIT -h 2>&1 || true)
    grep -q 'USAGE:' <<<"$OUTPUT"
    grep -q 'FLAGS:' <<<"$OUTPUT"
    grep -q 'Export resources owned by a tenant' <<<"$OUTPUT"
}

function test_version {
    local OUTPUT=$($TMIT --version 2>&1 || true)
    local EXPECTED_VERSION=$(grep '^version' Cargo.toml | awk '-F"' '{ print $2; }')
    grep -q "tmit $EXPECTED_VERSION" <<<"$OUTPUT"
}

rebuild_just_once
test_no_args
test_help
test_version
