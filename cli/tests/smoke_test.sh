#!/bin/bash

set -euxo pipefail

SCRIPT_DIR=$(dirname $(realpath "$0"))

$SCRIPT_DIR/smoke/usage.sh
$SCRIPT_DIR/smoke/export.sh

set +x
echo "CLI SMOKE TESTS OK"
