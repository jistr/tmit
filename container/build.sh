#!/bin/bash

set -euxo pipefail

dnf -y install git cargo make openssl-devel rust

git clone https://gitlab.com/jistr/tmit /root/tmit
pushd /root/tmit
make build-cli
cp /root/tmit/cli/target/release/tmit /usr/local/bin/tmit
popd

rm -rf /root/tmit
rm -rf /root/.cargo
dnf -y remove cargo make openssl-devel rust
dnf clean all
