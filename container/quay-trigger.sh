#!/bin/bash

set -euxo pipefail

if [ -z "${TMIT_QUAY_WEBHOOK_URL:-}" ]; then
    echo >/dev/fd/2 "ERROR: Env variable TMIT_QUAY_WEBHOOK_URL must be set."
    exit 1
fi

TMIT_IMAGE="${TMIT_IMAGE:-quay.io/jistr/tmit}"
TMIT_BRANCH="${TMIT_BRANCH:-master}"
TMIT_IMAGE_BRANCH_TAG="$TMIT_IMAGE:$TMIT_BRANCH"
TMIT_REPO_URL="${TMIT_REPO_URL:-https://gitlab.com/jistr/tmit.git}"

# delete the image to force rebuild
if skopeo inspect docker://"$TMIT_IMAGE_BRANCH_TAG" &> /dev/null; then
    skopeo delete docker://"$TMIT_IMAGE_BRANCH_TAG"
fi

TMIT_BRANCH_REF="refs/heads/$TMIT_BRANCH"
TMIT_BRANCH_COMMIT=$(git ls-remote "$TMIT_REPO_URL" \
                         | grep "$TMIT_BRANCH_REF" \
                         | awk '{ print $1; }')

curl \
    --fail \
    -X POST \
    -H 'Content-Type: application/json' \
    -d '{ "commit": "'"$TMIT_BRANCH_COMMIT"'", "ref": "'"$TMIT_BRANCH_REF"'", "default_branch": "master" }' \
    "$TMIT_QUAY_WEBHOOK_URL"
