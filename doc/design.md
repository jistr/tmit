High-level development goals
----------------------------

* Simplicity. CLI-only, at least initially. The architecture has
  separate library and executable parts, which would in theory allow
  building a different interface than CLI as well, but we probably
  don't need to hurry that way. Due to IO-based design, the CLI should
  be very interoperable on its own (see below).

* I/O-based. Fetch metadata and/or content from source cloud -> write
  them as outputs -> read them as inputs -> push it to the destination
  cloud. This allows other tools to enter the phase between the "write
  output" and "read input".

  * E.g. arbitrary/custom yaml-parsing tools can be used as a smart
    filter to analyze the exported contents from source cloud and
    choose what (not) to import into the destination cloud.

* Logging. When contributing code, don't forget to add logging. Try to
  think about what can potentially fail and what kind of log output
  might help figuring out what went wrong, and on the contrary, what
  kind of log output would add clutter without much information value.

* Testing. Unit-test semantics of small parts wherever possible. Don't
  test aspects covered by Rust's safety guarantees. Emphasize
  automated functional tests (end-to-end, talking to a real OpenStack
  backend). Functional tests are closest to the real use cases and
  provide at least basic level of comfort that the software does what
  it should.

* Always talking to OpenStack via API. The tool must be able to be
  deployed externally to both source and destination clouds. No
  looking at DBs or other hacks. If we hit a hurdle due to no-hacks
  approach, it could mean that OpenStack's capabilities for
  tenant-to-tenant content migration are lacking, and an RFE for
  OpenStack might be required.

* Idempotency where possible. When a command fails, it should be
  possible to retry with the same command.

* Whenever simplicity / understandability / clarity gets into conflict
  with convenience / ease-of-use, we prefer simplicity /
  understandability / clarity. Prefer running 20 CLI commands to do
  something where each CLI command is simple and human can enter the
  process by amending inputs/outputs e.g. with additional tools or a
  text editor, rather than one magical command which aims to satisfy
  all use cases and eventually turns out to satisfy very few, and
  tends to fail in mysterious ways with partial completion and limited
  re-runnability.

* Tmit is intended to be a building block for tenant migrations rather
  than a push-button solution. The assumption is that to cover needs
  of a particular tenant migration, a knowledgeable human is running
  Tmit manually or has written additional automation around it.

Details
-------

Export resources:

    tmit export --os-cloud from-cloud [--project asdf] [--project-domain ghij] --resource-type network [--only-metadata] --out-dir resources

Import resources:

    tmit import --os-cloud to-cloud --in-dir resources
    # or
    tmit import --os-cloud to-cloud --in-dir resources/network.yaml

Validate resources (later iteration):

    tmit validate --in-dir resources
    # or
    tmit validate --in-dir resources/networks.yaml

Check if resources exist (later iteration):

    tmit check --os-cloud to-cloud --in-dir resources
    # or
    tmit check --os-cloud to-cloud --in-dir resources/network.yaml

The resources.yaml file contains resource metadata, and if some of the
resources contain BLOB data, it will be stored in separate files under
`resources/<resource-type>.data` directory and referenced from
`resources/<resource-type>.yaml`.

The `--only-metadata` parameter means that no BLOBs will be
downloaded, just metadata.

For `--in-dir` and `--out-dir` parameters, `-` can be passed as value,
meaning stdout/stdin. This implies `--only-metadata`.

Resources.yaml format:

    - type: network
      name: my_net
      os_network_api_version: v2
      os_project: asdf
      os_project_domain: ghij
    - type: network
      name: my_net_2
      os_network_api_version: v2
      os_project: asdf
      os_project_domain: ghij

Challenges:

* Admins can view resources of all projects, but cannot create
  resources in those projects (unless they have a role in the
  project). To create a resource under a project, either credentials
  of the target non-admin user have to be used for importing, or the
  admin user has to be added into the project for the duration of the
  import.

* OpenStack doesn't have strict requirements on resource naming
  (doesn't require non-empty, unique names). The migration tool will
  enforce names which are non-empty, and unique per resource
  type. This is necessary to allow idempotent imports, which is in
  turn necessary for retrying imports on failure. The tool should
  allow to export/serialize resources that are not properly named, but
  the exported data should fail validation and be refused when fed
  into the import command.

Code structure:

    tmit::export::
    tmit::import::
    tmit::check::
    tmit::validation::  - validate resources.yaml (and linked blobs)

    tmit::network::Network  - struct, serde-able to resources.yaml
    tmit::network::fetch_networks
    tmit::network::create_network
    tmit::network::create_networks
    tmit::network::check_network
    tmit::network::check_networks     - check if networks exist
    tmit::network::validate_networks  - any network-specific validation
                                        to run in tmit::validation

Resource checklist:

* network

  * Network

  * Subnet

  * Router

  * SecurityGroup

  * (Port)

  * (FloatingIP)

* image

  * Image

* volume

  * Volume

* compute

  * Flavor

  * KeyPair

  * Server
