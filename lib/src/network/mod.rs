mod network;
mod subnet;

pub use network::{
    Network,
    fetch_networks,
    create_network,
    create_networks,
};

pub use subnet::{
    Subnet,
    fetch_subnets,
    create_subnet,
    create_subnets,
};

// reexport the struct from OpenStack, it's unlikely to change. If it
// does we can implement a custom one here and convert between them.
pub use openstack::network::AllocationPool;
