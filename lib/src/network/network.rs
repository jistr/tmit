use log::{debug, info};
use openstack::Cloud;
use openstack::network::{Network as OsNetwork, NewNetwork as OsNewNetwork};
use osauth::Session;
use serde::{Serialize, Deserialize};

use crate::result::*;

#[derive(Debug, Deserialize, Serialize)]
pub struct Network {
    pub id: String,
    pub name: Option<String>,

    pub admin_state_up: bool,
    // pub availability_zones: Vec<String>,
    pub description: Option<String>,
    pub dns_domain: Option<String>,
    pub external: Option<bool>,
    pub is_default: Option<bool>,
    pub mtu: Option<u32>,
    pub port_security_enabled: Option<bool>,
    pub shared: bool,
    // pub vlan_transparent: Option<bool>,
}

impl Network {
    pub fn blank() -> Self {
        Network {
            id: String::new(),
            name: None,
            admin_state_up: true,
            description: None,
            dns_domain: None,
            external: None,
            is_default: None,
            mtu: None,
            port_security_enabled: None,
            shared: false,
        }
    }

    pub fn to_os(&self, session: &Session) -> OsNewNetwork {
        let cloud: Cloud = session.clone().into();
        let mut net = cloud.new_network();

        net.set_admin_state_up(self.admin_state_up);
        net.set_shared(self.shared);
        if let Some(ref description) = self.description {
            net.set_description(description.clone());
        }
        if let Some(ref dns_domain) = self.dns_domain {
            net.set_dns_domain(dns_domain.clone());
        }
        if let Some(external) = self.external {
            net.set_external(external);
        }
        if let Some(is_default) = self.is_default {
            net.set_default(is_default);
        }
        if let Some(mtu) = self.mtu {
            net.set_mtu(mtu);
        }
        if let Some(ref name) = self.name {
            net.set_name(name.clone());
        }
        if let Some(port_security_enabled) = self.port_security_enabled {
            net.set_port_security_enabled(port_security_enabled);
        }
        net
    }

    pub fn required_name(&self) -> Result<&String> {
        match self.name {
            Some(ref name) => Ok(name),
            None => Err(Error::new(ErrorKind::ValidationError,
                                   format!("Network '{}' is unnamed", self.id)))
        }
    }

    pub fn validate(&self) -> Result<()> {
        self.required_name()?;
        Ok(())
    }
}

impl From<OsNetwork> for Network {
    fn from(os_network: OsNetwork) -> Self {
        // We have to use clone() on properties because OsNetwork
        // doesn't support destructuring.
        Network {
            id: os_network.id().clone(),
            name: os_network.name().clone(),

            admin_state_up: os_network.admin_state_up(),
            description: os_network.description().clone(),
            dns_domain: os_network.dns_domain().clone(),
            external: os_network.external(),
            is_default: os_network.is_default(),
            mtu: os_network.mtu(),
            port_security_enabled: os_network.port_security_enabled(),
            shared: os_network.shared(),
        }
    }
}

pub fn fetch_networks(session: &Session) -> Result<Vec<Network>> {
    let cloud: Cloud = session.clone().into();
    let result_nets: Vec<Network> = cloud.list_networks()?.into_iter()
        .map(|os_net| os_net.into()).collect();
    debug!("Fetched networks: {:?}", result_nets);
    Ok(result_nets)
}

pub fn create_networks(session: &Session, networks: &[Network]) -> Result<()> {
    for network in networks {
        create_network(session, network)?;
    }
    Ok(())
}

pub fn create_network(session: &Session, network: &Network) -> Result<()> {
    network.validate()?;
    if ! exists_network(session, network)? {
        info!("Creating network '{}'.", network.required_name()?);
        debug!("Creating {:?}.", network);
        network.to_os(session).create()?;
    } else {
        debug!("Network '{}' exists, skipping creation.", network.required_name()?);
    }
    Ok(())
}

pub fn exists_network(session: &Session, network: &Network) -> Result<bool> {
    let cloud: Cloud = session.clone().into();
    Ok(cloud.find_networks().with_name(network.required_name()?).all()?.len() > 0)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_network_validate() {
        let mut network = Network::blank();
        network.description = Some("invalid network by Tmit standards".to_string());
        assert!(network.validate().is_err());

        network.name = Some("some name".to_string());
        assert!(network.validate().is_ok());
    }
}
