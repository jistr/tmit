use ipnet::IpNet;
use log::{debug, info};
use openstack::Cloud;
use openstack::network::{Subnet as OsSubnet, NewSubnet as OsNewSubnet};
use osauth::Session;
use serde::{Serialize, Deserialize};
use std::convert::{TryFrom, TryInto};

use crate::result::*;
use crate::network::AllocationPool;

#[derive(Debug, Deserialize, Serialize)]
pub struct Subnet {
    pub allocation_pools: Vec<AllocationPool>,
    pub cidr: Option<IpNet>,
    pub description: Option<String>,
    pub name: Option<String>,
    pub network_name: Option<String>,

    // dhcp_enabled
    // dns_nameservers[]
    // gateway_ip
    // host_routes[]
    // . destination
    // . next_hop
    // ip_version
    // ipv6_address_mode
    // ipv6_ra_mode
    // network_id <ref>
}

impl Subnet {
    pub fn blank() -> Self {
        Subnet {
            allocation_pools: vec![],
            cidr: None,
            description: None,
            name: None,
            network_name: None,
        }
    }

    pub fn to_os(&self, session: &Session) -> Result<OsNewSubnet> {
        let cloud: Cloud = session.clone().into();
        let mut subnet = cloud.new_subnet(
            self.required_network_name()?.as_str(),
            self.required_cidr()?.clone(),
        );
        for pool in self.allocation_pools.iter() {
            subnet.add_allocation_pool(pool.clone());
        }
        if let Some(ref description) = self.description {
            subnet.set_description(description.clone());
        }
        if let Some(ref name) = self.name {
            subnet.set_name(name.clone());
        }
        Ok(subnet)
    }

    pub fn required_name(&self) -> Result<&String> {
        match self.name {
            Some(ref name) => Ok(name),
            None => Err(Error::new(ErrorKind::ValidationError,
                                   format!("Subnet is unnamed: '{:?}'", self)))
        }
    }

    pub fn required_cidr(&self) -> Result<&IpNet> {
        match self.cidr {
            Some(ref cidr) => Ok(cidr),
            None => Err(Error::new(ErrorKind::ValidationError,
                                   format!("Subnet has no CIDR: '{:?}'", self)))
        }
    }

    pub fn required_network_name(&self) -> Result<&String> {
        match self.network_name {
            Some(ref network_name) => Ok(network_name),
            None => Err(Error::new(ErrorKind::ValidationError,
                                   format!("Subnet has no network name: '{:?}'", self)))
        }
    }

    pub fn validate(&self) -> Result<()> {
        self.required_name()?;
        self.required_cidr()?;
        self.required_network_name()?;
        Ok(())
    }
}

impl TryFrom<OsSubnet> for Subnet {
    type Error = crate::Error;

    fn try_from(os_subnet: OsSubnet) -> Result<Self> {
        // We have to use clone() on properties because OsSubnet
        // doesn't support destructuring.
        Ok(Subnet {
            allocation_pools: os_subnet.allocation_pools().clone(),
            cidr: Some(os_subnet.cidr().clone()),
            name: os_subnet.name().clone(),
            description: os_subnet.description().clone(),
            network_name: os_subnet.network()?.name().clone(),
        })
    }
}

pub fn fetch_subnets(session: &Session) -> Result<Vec<Subnet>> {
    let cloud: Cloud = session.clone().into();
    let result_nets: Result<Vec<Subnet>> = cloud.list_subnets()?.into_iter()
        .map(|os_net| os_net.try_into()).collect();
    debug!("Fetched subnets: {:?}", result_nets);
    result_nets
}

pub fn create_subnets(session: &Session, subnets: &[Subnet]) -> Result<()> {
    for subnet in subnets {
        create_subnet(session, subnet)?;
    }
    Ok(())
}

pub fn create_subnet(session: &Session, subnet: &Subnet) -> Result<()> {
    subnet.validate()?;
    if ! exists_subnet(session, subnet)? {
        info!("Creating subnet '{}'.", subnet.required_name()?);
        debug!("Creating {:?}.", subnet);
        subnet.to_os(session)?.create()?;
    } else {
        debug!("Subnet '{}' exists, skipping creation.", subnet.required_name()?);
    }
    Ok(())
}

pub fn exists_subnet(session: &Session, subnet: &Subnet) -> Result<bool> {
    let cloud: Cloud = session.clone().into();
    Ok(cloud.find_subnets().with_name(subnet.required_name()?).all()?.len() > 0)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_subnet_validate() {
        let mut subnet = Subnet::blank();
        subnet.description = Some("invalid subnet by Tmit standards".to_string());
        assert!(subnet.validate().is_err());
        subnet.name = Some("some name".to_string());
        assert!(subnet.validate().is_err());
        subnet.network_name = Some("some network name".to_string());
        assert!(subnet.validate().is_err());
        subnet.cidr = Some("192.168.0.0/24".parse().expect("Malformed CIDR"));
        assert!(subnet.validate().is_ok());
    }
}
