use serde::{Serialize, Deserialize};

use crate::network::{Network, Subnet};

#[derive(Debug, Deserialize, Serialize)]
#[serde(tag = "type")]
pub enum Resource {
    Network(Network),
    Subnet(Subnet),
}

impl From<Network> for Resource {
    fn from(inner: Network) -> Self {
        Resource::Network(inner)
    }
}

impl From<Subnet> for Resource {
    fn from(inner: Subnet) -> Self {
        Resource::Subnet(inner)
    }
}
