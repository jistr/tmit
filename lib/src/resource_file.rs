use serde::{Serialize, Deserialize};

use crate::Resource;
use crate::network::{Network, Subnet};

const CARGO_PKG_VERSION: &'static str = env!("CARGO_PKG_VERSION");

#[derive(Debug, Deserialize, Serialize)]
pub struct ResourceFile {
    pub tmit_version: String,
    pub resources: Vec<Resource>,
}

impl ResourceFile {
    pub fn new(resources: Vec<Resource>) -> Self {
        ResourceFile {
            tmit_version: CARGO_PKG_VERSION.to_string(),
            resources: resources,
        }
    }

    pub fn new_empty() -> Self {
        Self::new(vec![])
    }

    pub fn push_networks(&mut self, networks: impl Iterator<Item = Network>) {
        for network in networks {
            self.resources.push(network.into());
        }
    }

    pub fn push_subnets(&mut self, subnets: impl Iterator<Item = Subnet>) {
        for subnet in subnets {
            self.resources.push(subnet.into());
        }
    }
}
