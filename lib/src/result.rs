#[derive(Debug, Clone)]
pub enum ErrorKind {
    /// Error from OpenStack API or client
    OpenstackError(::osauth::Error),
    ValidationError,
    __Nonexhaustive,
}

#[derive(Debug, Clone)]
pub struct Error {
    kind: ErrorKind,
    message: String,
}

impl Error {
    pub fn new<S: Into<String>>(kind: ErrorKind, message: S) -> Error {
        Error {
            kind: kind,
            message: message.into(),
        }
    }
}

impl ErrorKind {
    pub fn name(&self) -> &'static str {
        match self {
            ErrorKind::OpenstackError(_) => "OpenstackError",
            _ => unreachable!(),
        }
    }

    pub fn inner_part_fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        match self {
            ErrorKind::OpenstackError(inner) => write!(f, " ({})", inner),
            _ => Ok(()),
        }
    }
}

impl ::std::fmt::Display for ErrorKind {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "{}", self.name())?;
        self.inner_part_fmt(f)
    }
}

impl ::std::fmt::Display for Error {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "{}: {}", self.kind.name(), self.message)?;
        self.kind.inner_part_fmt(f)
    }
}

impl ::std::error::Error for Error {
    fn source(&self) -> Option<&(dyn ::std::error::Error + 'static)> {
        match &self.kind {
            ErrorKind::OpenstackError(ref inner) => Some(inner),
            _ => None,
        }
    }
}

impl From<::osauth::Error> for Error {
    fn from(error: ::osauth::Error) -> Self {
        Self::new(ErrorKind::OpenstackError(error), "Error performing OpenStack operation")
    }
}

pub type Result<T> = ::std::result::Result<T, Error>;
