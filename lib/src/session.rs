use osauth::Session;

use crate::Result;

pub fn create_session(os_cloud: &str) -> Result<Session> {
    Ok(osauth::from_config(os_cloud)?)
}
