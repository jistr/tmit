// Fixtures are referenced from both CLI and lib, but individual
// fixtures might be used from just one, so we allow dead code.
#![allow(dead_code)]

use openstack::Cloud;
use openstack::network::{
    AllocationPool as OsAllocPool,
    HostRoute as OsHostRoute,
    Network as OsNetwork,
    Subnet as OsSubnet,
};
use osauth::Session;
use waiter::Waiter;

pub fn seed_network(sessions: &[&Session]) {
    for session in sessions {
        let cloud: Cloud = (*session).clone().into();
        cloud.new_network()
            .with_name("tmit_net")
            .with_admin_state_up(false)
            .with_description("tmit test net")
            // devstack doesn't have dns-integration extension
            // .with_dns_domain("example.org")
            .with_external(false)
            .with_default(false)
            .with_mtu(1350)
            .with_port_security_enabled(false)
            .with_shared(false)
            .create().expect("Failed to seed tmit_net network");
    }
}

pub fn clean_network(sessions: &[&Session]) {
    for session in sessions {
        let cloud: Cloud = (*session).clone().into();
        cloud.find_networks().with_name("tmit_net").all().ok().and_then(
            |nets| {
                for net in nets.into_iter() {
                    net.delete().expect("Failed to request tmit_net deletion")
                        .wait().expect("Timed out deleting tmit_net");
                }
                None as Option<Vec<OsNetwork>>
            });
        cloud.find_networks().with_name("tmit_net").all().ok().and_then(
            |nets| {
                assert!(nets.len() == 0, "Network tmit_net wasn't cleaned up");
                None as Option<Vec<OsNetwork>>
            });
    }
}

pub fn seed_subnet(sessions: &[&Session]) {
    for session in sessions {
        let cloud: Cloud = (*session).clone().into();
        cloud.new_subnet("tmit_net", "192.168.5.0/24".parse().unwrap())
            .with_name("tmit_subnet")
            .with_allocation_pool(OsAllocPool {
                start: "192.168.5.10".parse().unwrap(),
                end: "192.168.5.20".parse().unwrap(),
            })
            .with_allocation_pool(OsAllocPool {
                start: "192.168.5.110".parse().unwrap(),
                end: "192.168.5.120".parse().unwrap(),
            })
            .with_description("tmit test subnet")
            .with_dhcp_enabled(false)
            .with_dns_nameserver("192.168.5.8")
            .with_dns_nameserver("192.168.5.9")
            .with_host_route(OsHostRoute {
                destination: "192.168.6.0/24".parse().unwrap(),
                next_hop: "192.168.5.6".parse().unwrap(),
            })
            .create().expect("Failed to seed tmit_subnet subnet");
    }
}

pub fn clean_subnet(sessions: &[&Session]) {
    for session in sessions {
        let cloud: Cloud = (*session).clone().into();
        cloud.find_subnets().with_name("tmit_subnet").all().ok().and_then(
            |subnets| {
                for subnet in subnets.into_iter() {
                    subnet.delete().expect("Failed to request tmit_subnet deletion")
                        .wait().expect("Timed out deleting tmit_subnet");
                }
                None as Option<Vec<OsSubnet>>
            });
        cloud.find_subnets().with_name("tmit_subnet").all().ok().and_then(
            |subnets| {
                assert!(subnets.len() == 0, "Subnet tmit_subnet wasn't cleaned up");
                None as Option<Vec<OsSubnet>>
            });
    }
}
