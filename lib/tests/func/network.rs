use tmit::network::{self, Network};

use crate::func::util;
use crate::fixture::{
    clean_network,
    seed_network,
};

#[test]
fn test_migrate_network() {
    let (src_session, dst_session) = util::prep_sessions();
    clean_network(&[&src_session, &dst_session]);
    seed_network(&[&src_session]);

    let src_networks = network::fetch_networks(&src_session)
        .expect("Failed to fetch networks from source cloud");
    network::create_networks(&dst_session, &src_networks)
        .expect("Failed to create networks in destination cloud");
    network::create_networks(&dst_session, &src_networks)
        .expect("Failed idempotency test on network creation");
    let all_nets = network::fetch_networks(&dst_session)
        .expect("Failed to fetch networks from destination cloud");
    let all_tmit_nets: Vec<&Network> = all_nets.iter()
        .filter(|net| net.name.as_ref().expect("Unnamed network found") == "tmit_net")
        .collect();
    assert_eq!(all_tmit_nets.len(), 1,
               "Network tmit_net wasn't found in destination cloud exactly once");
    let tmit_net = all_tmit_nets[0];
    assert_eq!(tmit_net.admin_state_up, false, "Incorrect admin_state_up");
    assert_eq!(tmit_net.description, Some("tmit test net".into()), "Incorrect description");
    assert_eq!(tmit_net.external, Some(false), "Incorrect 'external'");
    // for some reason non-default nets get represented with None instead of Some(false)
    assert_eq!(tmit_net.is_default, None, "Incorrect is_default");
    assert_eq!(tmit_net.mtu, Some(1350), "Incorrect mtu");
    assert_eq!(tmit_net.port_security_enabled, Some(false), "Incorrect port_security_enabled");
    assert_eq!(tmit_net.shared, false, "Incorrect shared");

    clean_network(&[&src_session, &dst_session]);
}

#[test]
fn test_create_invalid_network() {
    let (_src_session, dst_session) = util::prep_sessions();
    let mut network = Network::blank();
    network.description = Some("invalid network by Tmit standards".to_string());
    let result = network::create_network(&dst_session, &network);
    assert!(result.is_err());
}
