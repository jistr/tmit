use tmit::network::{self, AllocationPool, Subnet};

use crate::func::util;
use crate::fixture::{
    clean_network,
    clean_subnet,
    seed_network,
    seed_subnet,
};

#[test]
fn test_migrate_subnet() {
    let (src_session, dst_session) = util::prep_sessions();
    clean_subnet(&[&src_session, &dst_session]);
    clean_network(&[&src_session, &dst_session]);

    seed_network(&[&src_session, &dst_session]);
    seed_subnet(&[&src_session]);

    // === SUBNET ===
    let src_subnets = network::fetch_subnets(&src_session)
        .expect("Failed to fetch subnets from source cloud");
    network::create_subnets(&dst_session, &src_subnets)
        .expect("Failed to create subnets in destination cloud");
    network::create_subnets(&dst_session, &src_subnets)
        .expect("Failed idempotency test on subnet creation");
    let all_subnets = network::fetch_subnets(&dst_session)
        .expect("Failed to fetch subnets from destination cloud");
    let all_tmit_subnets: Vec<&Subnet> = all_subnets.iter()
        .filter(|sub| sub.name.as_ref().expect("Unnamed subnet found") == "tmit_subnet")
        .collect();
    assert_eq!(all_tmit_subnets.len(), 1,
               "Subnet tmit_subnet wasn't found in destination cloud exactly once");
    let tmit_subnet = all_tmit_subnets[0];
    assert_eq!(tmit_subnet.name, Some("tmit_subnet".to_string()));
    assert_eq!(tmit_subnet.description, Some("tmit test subnet".to_string()));
    assert_eq!(tmit_subnet.network_name, Some("tmit_net".to_string()));
    assert!(tmit_subnet.allocation_pools.contains(
        &AllocationPool {
            start: "192.168.5.10".parse().unwrap(),
            end: "192.168.5.20".parse().unwrap(),
        }));
    assert!(tmit_subnet.allocation_pools.contains(
        &AllocationPool {
            start: "192.168.5.110".parse().unwrap(),
            end: "192.168.5.120".parse().unwrap(),
        }));

    clean_subnet(&[&src_session, &dst_session]);
    clean_network(&[&src_session, &dst_session]);
}

#[test]
fn test_create_invalid_subnet() {
    let (_src_session, dst_session) = util::prep_sessions();
    let mut subnet = Subnet::blank();
    subnet.description = Some("invalid subnet by Tmit standards".to_string());
    let result = network::create_subnet(&dst_session, &subnet);
    assert!(result.is_err());
}
