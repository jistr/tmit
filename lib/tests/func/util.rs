use env_logger;
use osauth::{self, Session};
use std::env;

pub fn prep_sessions() -> (Session, Session) {
    enable_logging();
    (src_session(), dst_session())
}

pub fn enable_logging() {
    let _ = env_logger::builder().is_test(true).try_init();
}

pub fn src_session() -> Session {
    let src_cloud = env::var("TEST_SRC_OS_CLOUD").unwrap_or("devstack".to_string());
    osauth::from_config(&src_cloud).expect("Failed to create test source cloud session.")
}

pub fn dst_session() -> Session {
    let dst_cloud = env::var("TEST_DST_OS_CLOUD").unwrap_or("devstack-alt".to_string());
    osauth::from_config(&dst_cloud).expect("Failed to create test source cloud session.")
}
